<?php
defined('BASEPATH') OR exit('No direct script access allowed');



$route['default_controller'] = "login";
$route['404_override'] = 'error';


/*********** USER DEFINED ROUTES *******************/


$route['edlo'] = 'user/edlo';


$route['Destination'] = 'user/Destination';

$route['addNewstaff'] = 'user/addNewstaff';
$route['addlocation'] = 'user/addlocation';
$route['awbform'] = 'user/awbform';
$route['addtax'] = 'user/addtax';
$route['addlocation'] = 'user/addlocation';
$route['addgst'] = 'user/addgst';
$route['addexchange'] = 'user/addexchange';


$route['staff'] = 'user/staff';
$route['awbform'] = 'user/awbform';
$route['stategst'] = 'user/getgst';
$route['location'] = 'user/location';
$route['tax'] = 'user/tax';
$route['exchangerate'] = 'user/exchangerate';
$route['Search'] = 'user/Search';
$route['report'] = 'user/report';
$route['All_Report'] = 'user/All_Report';



$route['shipmentdetails'] = 'user/shipmentdetails';



$route['Detail_report'] = 'user/Detail_report';
$route['location_report'] = 'user/location_report';


//SAIF

$route['dashboard1'] = 'User/dashboard1';
$route['dashboard2'] = 'User/dashboard2';
$route['brach_manager'] = 'User/brach_manager';
$route['Branch_manager_form'] = 'User/Branch_manager_form';



//SAIF







































$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['logout'] = 'user/logout';
$route['userListing'] = 'user/userListing';
$route['userListing/(:num)'] = "user/userListing/$1";
$route['addNew'] = "user/addNew";
$route['addNewUser'] = "user/addNewUser";
$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";
$route['deleteUser'] = "user/deleteUser";
$route['profile'] = "user/profile";
$route['profile/(:any)'] = "user/profile/$1";
$route['profileUpdate'] = "user/profileUpdate";
$route['profileUpdate/(:any)'] = "user/profileUpdate/$1";

$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['changePassword/(:any)'] = "user/changePassword/$1";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";
$route['login-history'] = "user/loginHistoy";
$route['login-history/(:num)'] = "user/loginHistoy/$1";
$route['login-history/(:num)/(:num)'] = "user/loginHistoy/$1/$2";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";

/* End of file routes.php */
/* Location: ./application/config/routes.php */