<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo "Airway Management"; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/index.jpg">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
    <style>
    	.error{
    		color:red;
    		font-weight: normal;
    	}


    </style>
    

    <style>
  #backButton {
    border-radius: 4px;
    padding: 8px;
    border: none;
    font-size: 16px;
    background-color: #2eacd1;
    color: white;
    position: absolute;
    top: 10px;
    left: 50px;
    cursor: pointer;

  }

  .invisible {
    display: none;
  }
  .lefted
  {
    float:left;
    margin-left:20px;

  }
</style>

    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      
      <header class="main-header" style="background-color:red;">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo"style="background-color:#222d32;">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"style="background-color:#222d32;"><b>Airway Management</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation" 
         style="background-color:#222d32;">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                  <i class="fa fa-history"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"> Last Login : <i class="fa fa-clock-o"></i> <?= empty($last_login) ? "First Time Login" : $last_login; ?></li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $name;  ?></span>
                  
                </a>
                <ul class="dropdown-menu" >
                  <!-- User image -->
                  <li class="user-header"style="background-color:#222d32;">
                    
                    <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $name . "</br>"  ?> 
                      <?php echo "". $this->session->userdata('LocationName'); ?>
                      
                    </p>
                  </br>
                  <p>
                    
                    
                  </p>
                    
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer" style="background-color:  #C0C0C0; ">
                    <div class="pull-left">
                      <a href="<?php echo base_url(); ?>profile" class="btn btn-warning btn-flat"><i class="fa fa-user-circle"></i> Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
           <br>
           



<?php
            if($role == ROLE_EMPLOYEE)
            {
            ?>

            <li class="treeview">
              <a href="<?php echo base_url(); ?>dashboard2">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>

<li class="treeview"><a href="<?php echo base_url('brach_manager'); ?>?User=<?php  echo $this->session->userdata('Locationid'); ?>">
  <i class="fa fa-search"></i><span> Airway Bill</span> </a>
</li>


            <?php
            }




            ?>


            <?php
            if($role == ROLE_MANAGER)
            {
            ?>


                          <li class="treeview">
                            <a href="<?php echo base_url(); ?>dashboard1">
                              <i class="fa fa-dashboard"></i> <span>Dashboard</span> </i>
                            </a>
                          </li>


                          <li class="treeview"><a href="<?php echo base_url('Search'); ?>?User=<?php  echo $this->session->userdata('my_super_array'); ?>">
                            <i class="fa fa-search"></i><span> Airway Bill</span> </a>
                          </li>


                            <li class="treeview"><a href="<?php echo base_url(); ?>stategst">
                              <i style="font-size:16px" class="fa fa-adn"></i><span> State Gst </span></a>
                            </li>

                              <!-- <li class="treeview"><a href="<?php echo base_url(); ?>Destination">
                              <i style="font-size:16px" class="fa fa-telegram"></i><span>Destination </span></a></li> -->

                            <li class="treeview"><a href="<?php echo base_url(); ?>location">
                              <i style="font-size:16px" class="fa">&#xf041;</i><span> Location </span></a></li>

                           
                              <!-- <li class="treeview"><a href="<?php echo base_url(); ?>tax ">

                                <i class="fa fa-tumblr-square" ></i>
                                 <span>Gst </span>
                               </a>
                              </li> -->


                            <li class="treeview"><a href="<?php echo base_url(); ?>exchangerate">

                              <i class="fa fa-edge"></i>
                              <span> Exchange Rate </span>
                            </a></li>



            <?php
            }
            ?>



            <?php
            if($role == ROLE_ADMIN  )
            {
            ?>


                          <li class="treeview">
                            <a href="<?php echo base_url(); ?>dashboard">
                              <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
                            </a>
                          </li>
            



    
<!--<li class="treeview">
  <a href="<?php echo base_url(); ?>awbform"><i class="fa fa-adn"></i><span> AWB form </span></a></li>-->

                 <li class="treeview"><a href="<?php echo base_url('Search'); ?>?User=<?php  echo $this->session->userdata('my_super_array'); ?>">
                            
                            <i class="fa fa-search"></i><span> Airway Bill</span> </a></li>


                            <li class="treeview"><a href="<?php echo base_url(); ?>stategst">
                              <i style="font-size:16px" class="fa fa-adn"></i><span> State Gst </span></a></li>

                              <!-- <li class="treeview"><a href="<?php echo base_url(); ?>Destination">
                              <i style="font-size:16px" class="fa fa-telegram"></i><span>Destination </span></a></li> -->

                            <li class="treeview"><a href="<?php echo base_url(); ?>location">
                              <i style="font-size:16px" class="fa">&#xf041;</i><span> Location </span></a></li>

                           
                              <!-- <li class="treeview"><a href="<?php echo base_url(); ?>tax ">

                                <i class="fa fa-tumblr-square" ></i>
                                 <span>Gst </span>
                               </a>
                              </li> -->


                            <li class="treeview"><a href="<?php echo base_url(); ?>exchangerate">

                              <i class="fa fa-edge"></i>
                              <span> Exchange Rate </span>
                            </a></li>

 




<li class="treeview">
              <a href="<?php echo base_url(); ?>userListing">
                <i class="fa fa-users"></i>
                <span>Users</span>
              </a>
            </li>

            <li class="treeview">
              <a href="<?php echo base_url(); ?>staff">
                <i class="fa fa-user-circle-o"></i>
                <span>Staff</span>
              </a>
            </li>

<ul class="sidebar-menu" data-widget="tree">

<li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Reports</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>

<ul class="treeview-menu">
<li><a href="<?php echo base_url(); ?>report"><i class="fa fa-circle-o"></i>
  Short Reports </a></li>

 <li><a href="<?php echo base_url(); ?>Detail_report"><i class="fa fa-circle-o"></i>
  Detail Reports </a></li>

  <li><a href="<?php echo base_url(); ?>All_Report"><i class="fa fa-circle-o"></i>
  All Report </a></li>

  <li><a href="<?php echo base_url(); ?>shipmentdetails"><i class="fa fa-circle-o"></i>
  Shipment Details Report </a></li>

  <li><a href="<?php echo base_url(); ?>location_report"><i class="fa fa-circle-o"></i>
  Location Reports </a></li>
</ul>


</li>



          <!--   <li class="treeview">
              <a href="<?php echo base_url(); ?>program" >
                <i class="fa fa-thumb-tack"></i>
                <span>Program</span>
              </a>
            </li> -->

         <!--    <li class="treeview">
              <a href="<?php echo base_url(); ?>module" >
                <i class="fa fa-thumb-tack"></i>
                <span>Modules</span>
              </a>
            </li> -->
          <!--   <li class="treeview">
              <a href="<?php echo base_url('course') ?>" >
                <i class="fa fa-thumb-tack"></i>
                <span>Courses</span>
              </a>
            </li> -->
            <!--  <li class="treeview">
              <a href="<?php echo base_url('batch') ?>" >
                <i class="fa fa-thumb-tack"></i>
                <span>Batches</span>
              </a>
            </li> -->

            


            
          
            

               
<!-- <?php
            if($role==Branch_Manager)
            {
            ?>

<li class="treeview">
              <a href="<?php echo base_url(); ?>userListing">
                <i class="fa fa-users"></i>
                <span>Users</span>
              </a>
            </li>

            <li class="treeview">
              <a href="<?php echo base_url(); ?>staff">
                <i class="fa fa-user-circle-o"></i>
                <span>Staff</span>
              </a>
            </li>



 <?php
            }
            ?> -->
       


                        
<ul class="sidebar-menu" data-widget="tree">
                    
       


            <?php
            }
            ?>




          </ul>
          </ul>
</ul>

        </section>
        <!-- /.sidebar -->
      </aside>