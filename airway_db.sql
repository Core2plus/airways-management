-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2018 at 09:01 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `airway_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(100) NOT NULL,
  `name` tinytext,
  `pname` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `name`, `pname`) VALUES
(1, 'ki', 'Nwfp');

-- --------------------------------------------------------

--
-- Table structure for table `awbform`
--

CREATE TABLE `awbform` (
  `id` int(23) NOT NULL,
  `date` date DEFAULT NULL,
  `AWBID` varchar(141) DEFAULT NULL,
  `DP_ID` int(30) DEFAULT NULL,
  `CountryID` int(50) DEFAULT NULL,
  `Weight` float DEFAULT NULL,
  `LocationID` varchar(144) DEFAULT NULL,
  `Tax` int(244) DEFAULT NULL,
  `ActualPrice` int(244) DEFAULT NULL,
  `dicountper` float UNSIGNED DEFAULT NULL,
  `Discont` float DEFAULT NULL,
  `SellingPrice` float DEFAULT NULL,
  `DepositSlip` varchar(255) DEFAULT NULL,
  `DepositDate` date DEFAULT NULL,
  `BankReference` int(244) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `companyprofile`
--

CREATE TABLE `companyprofile` (
  `companyId` int(10) NOT NULL,
  `companyName` varchar(300) DEFAULT NULL,
  `aboutCompany` varchar(500) DEFAULT NULL,
  `foundingDate` date DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contactNo` varchar(12) DEFAULT NULL,
  `logo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companyprofile`
--

INSERT INTO `companyprofile` (`companyId`, `companyName`, `aboutCompany`, `foundingDate`, `website`, `address`, `email`, `contactNo`, `logo`) VALUES
(0, 'ICL Express', 'ICL Express is one of the Fastest Express & Freight Forwarding companies operating in Pakistan', '2018-11-16', 'www.interflow.com.pk', 'PECHS', 'Kamran.Iqbal@interflow.com.pk', '090099212', 'NotDefine');

-- --------------------------------------------------------

--
-- Table structure for table `countrysetup`
--

CREATE TABLE `countrysetup` (
  `CountryID` int(5) DEFAULT NULL,
  `CountryName` varchar(600) DEFAULT NULL,
  `CountryCode2D` varchar(15) DEFAULT NULL,
  `CountryCode3D` varchar(21) DEFAULT NULL,
  `DialingCode` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countrysetup`
--

INSERT INTO `countrysetup` (`CountryID`, `CountryName`, `CountryCode2D`, `CountryCode3D`, `DialingCode`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '4'),
(2, 'Albania', 'AL', 'ALB', '8'),
(3, 'Algeria', 'DZ', 'DZA', '12'),
(4, 'American Samoa', 'AS', 'ASM', '16'),
(5, 'Andorra', 'AD', 'AND', '20'),
(6, 'Angola', 'AO', 'AGO', '24'),
(7, 'Anguilla', 'AI', 'AIA', '660'),
(8, 'Antarctica', 'AQ', 'ATA', '10'),
(9, 'Antigua and Barbuda', 'AG', 'ATA', '28'),
(10, 'Argentina', 'AR', 'ARG', '32'),
(11, 'Armenia', 'AM', 'ARM', '51'),
(12, 'Aruba', 'AW', 'ABW', '533'),
(13, 'Australia', 'AU', 'AUS', '36'),
(14, 'Austria', 'AT', 'AUT', '40'),
(15, 'Azerbaijan', 'AZ', 'AZE', '31'),
(16, 'Bahamas', 'BS', 'BHS', '44'),
(17, 'Bahrain', 'BH', 'BHR', '48'),
(18, 'Bangladesh', 'BD', 'BGD', '50'),
(19, 'Barbados', 'BB', 'BRB', '52'),
(20, 'Belarus', 'BY', 'BLR', '112'),
(21, 'Belgium', 'BE', 'BEL', '56'),
(22, 'Belgium-Luxembourg', 'BE', 'BEL', '58'),
(23, 'Belize', 'BZ', 'BLZ', '84'),
(24, 'Benin', 'BJ', 'BEN', '204'),
(25, 'Bermuda', 'BM', 'BMU', '60'),
(26, 'Bhutan', 'BT', 'BTN', '64'),
(27, 'Bolivia ', 'BO', 'BOL', '68'),
(28, 'Bonaire', 'BQ', 'BES', '535'),
(29, 'Bosnia Herzegovina', 'BA', 'BIH', '70'),
(30, 'Botswana', 'BW', 'BWA', '72'),
(31, 'Bouvet Island', 'N/A', 'N/A', '74'),
(32, 'Br Antarctic Terr', 'N/A', 'N/A', '80'),
(33, 'Br Indian Ocean Terr', 'IO', 'IOT', '86'),
(34, 'Br Virgin Isds', 'VG', 'VGB', '92'),
(35, 'Brazil', 'BR', 'BRA', '76'),
(36, 'Brunei Darussalam', 'BN', 'BRN', '96'),
(37, 'Bulgaria', 'BG', 'BGR', '100'),
(38, 'Bunkers', 'N/A', 'N/A', '837'),
(39, 'Burkina Faso', 'BF', 'BFA', '854'),
(40, 'Burundi', 'BI', 'BDI', '108'),
(41, 'Cabo Verde', 'CV', 'CPV', '132'),
(42, 'CACM, nes', 'N/A', 'N/A', '471'),
(43, 'Cambodia', 'KH', 'KHM', '116'),
(44, 'Cameroon', 'CM', 'CMR', '120'),
(45, 'Canada', 'CA', 'CAN', '124'),
(46, 'Caribbean, nes', 'N/A', 'N/A', '129'),
(47, 'Cayman Isds', 'KY', 'CYM', '136'),
(48, 'Central African Rep.', 'CF', 'CAF', '140'),
(49, 'Chad', 'TD', 'TCD', '148'),
(50, 'Chile', 'CL', 'CHL', '152'),
(51, 'China', 'CN', 'CHN', '156'),
(52, 'Hong Kong SAR', 'HK', 'HKG', '344'),
(53, 'Macao SAR', 'MO', 'MAC', '446'),
(54, 'Christmas Isds', 'CX', 'CXR', '162'),
(55, 'Cocos Isds', 'CC', 'CCK', '166'),
(56, 'Colombia', 'CO', 'COL', '170'),
(57, 'Comoros', 'KM', 'COM', '174'),
(58, 'Congo', 'CG', 'COG', '178'),
(59, 'Cook Isds', 'CK', 'COK', '184'),
(60, 'Costa Rica', 'CR', 'CRI', '188'),
(61, 'C?te d\'Ivoire', 'CI', 'CIV', '384'),
(62, 'Croatia', 'HR', 'HRV', '191'),
(63, 'Cuba', 'CU', 'CUB', '192'),
(64, 'Cura?ao', 'CW', 'CUW', '531'),
(65, 'Cyprus', 'CY', 'CYP', '196'),
(66, 'Czechia', 'CZ', 'CZE', '203'),
(67, 'Czechoslovakia', 'CS', 'CSK', '200'),
(68, 'Dem. People\'s Rep. of Korea', 'KP', 'PRK', '408'),
(69, 'Dem. Rep. of the Congo', 'CD', 'COD', '180'),
(70, 'Denmark', 'DK', 'DNK', '208'),
(71, 'Djibouti', 'DJ', 'DJI', '262'),
(72, 'Dominica', 'DM', 'DMA', '212'),
(73, 'Dominican Rep.', 'DO', 'DOM', '214'),
(74, 'Pakistan', 'PK', 'PAK', '588'),
(75, 'Ecuador', 'EC', 'ECU', '218'),
(76, 'Egypt', 'EG', 'EGY', '818'),
(77, 'El Salvador', 'SV', 'SLV', '222'),
(78, 'Equatorial Guinea', 'GQ', 'GNQ', '226'),
(79, 'Eritrea', 'ER', 'ERI', '232'),
(80, 'Estonia', 'EE', 'EST', '233'),
(81, 'Ethiopia', 'ET', 'ETH', '231'),
(82, 'EU-28', 'EU', 'EU2', '97'),
(83, 'Europe EFTA, nes', 'N/A', 'N/A', '697'),
(84, 'Europe EU, nes', 'N/A', 'N/A', '492'),
(85, 'Faeroe Isds', 'FO', 'FRO', '234'),
(86, 'Falkland Isds (Malvinas)', 'FK', 'FLK', '238'),
(87, 'Fiji', 'FJ', 'FJI', '242'),
(88, 'Finland', 'FI', 'FIN', '246'),
(89, 'Fmr Arab Rep. of Yemen', 'YE', 'YEM', '886'),
(90, 'Fmr Dem. Rep. of Germany', 'DD', 'DDR', '278'),
(91, 'Fmr Dem. Rep. of Vietnam', 'VD', 'VDR', '866'),
(92, 'Fmr Dem. Yemen', 'YD', 'YMD', '720'),
(93, 'Fmr Ethiopia', 'ET', 'ETH', '230'),
(94, 'Fmr Fed. Rep. of Germany', 'DE', 'DEU', '280'),
(95, 'Fmr Pacific Isds', 'PC', 'PCI', '582'),
(96, 'Fmr Panama, excl.Canal Zone', 'PA', 'PAN', '590'),
(97, 'Fmr Panama-Canal-Zone', 'PZ', 'PCZ', '592'),
(98, 'Fmr Rep. of Vietnam', 'VN', 'VNM', '868'),
(99, 'Fmr Rhodesia Nyas', 'N/A', 'N/A', '717'),
(100, 'Fmr Sudan', 'SD', 'SDN', '736'),
(101, 'Fmr Tanganyika', 'N/A', 'N/A', '835'),
(102, 'Fmr USSR', 'SU', 'SUN', '810'),
(103, 'Fmr Yugoslavia', 'YU', 'YUG', '890'),
(104, 'Fmr Zanzibar and Pemba Isd', 'N/A', 'N/A', '836'),
(105, 'Fr. South Antarctic Terr.', 'FQ', 'ATF', '260'),
(106, 'France', 'FR', 'FRA', '251'),
(107, 'Free Zones', 'N/A', 'N/A', '838'),
(108, 'French Guiana', 'GF', 'GUF', '254'),
(109, 'French Polynesia', 'PF', 'PYF', '258'),
(110, 'FS Micronesia', 'FM', 'FSM', '583'),
(111, 'Gabon', 'GA', 'GAB', '266'),
(112, 'Gambia', 'GM', 'GMB', '270'),
(113, 'Georgia', 'GE', 'GEO', '268'),
(114, 'Germany', 'DE', 'DEU', '276'),
(115, 'Ghana', 'GH', 'GHA', '288'),
(116, 'Gibraltar', 'GI', 'GIB', '292'),
(117, 'Greece', 'GR', 'GRC', '300'),
(118, 'Greenland', 'GL', 'GRL', '304'),
(119, 'Grenada', 'GD', 'GRD', '308'),
(120, 'Guadeloupe', 'GP', 'GLP', '312'),
(121, 'Guam', 'GU', 'GUM', '316'),
(122, 'Guatemala', 'GT', 'GTM', '320'),
(123, 'Guinea', 'GN', 'GIN', '324'),
(124, 'Guinea-Bissau', 'GW', 'GNB', '624'),
(125, 'Guyana', 'GY', 'GUY', '328'),
(126, 'Haiti', 'HT', 'HTI', '332'),
(127, 'Heard Island and McDonald Islands', 'HM', 'HMD', '334'),
(128, 'Holy See (Vatican City State)', 'VA', 'VAT', '336'),
(129, 'Honduras', 'HN', 'HND', '340'),
(130, 'Hungary', 'HU', 'HUN', '348'),
(131, 'Iceland', 'IS', 'ISL', '352'),
(132, 'India', 'IN', 'IND', '699'),
(133, 'India, excl. Sikkim', 'IN', 'IND', '356'),
(134, 'Indonesia', 'ID', 'IDN', '360'),
(135, 'Iran', 'IR', 'IRN', '364'),
(136, 'Iraq', 'IQ', 'IRQ', '368'),
(137, 'Ireland', 'IE', 'IRL', '372'),
(138, 'Israel', 'IL', 'ISR', '376'),
(139, 'Italy', 'IT', 'ITA', '381'),
(140, 'Jamaica', 'JM', 'JAM', '388'),
(141, 'Japan', 'JP', 'JPN', '392'),
(142, 'Jordan', 'JO', 'JOR', '400'),
(143, 'Kazakhstan', 'KZ', 'KAZ', '398'),
(144, 'Kenya', 'KE', 'KEN', '404'),
(145, 'Kiribati', 'KI', 'KIR', '296'),
(146, 'Kuwait', 'KW', 'KWT', '414'),
(147, 'Kyrgyzstan', 'KG', 'KGZ', '417'),
(148, 'LAIA, nes', 'N/', 'N/A', '473'),
(149, 'Lao People\'s Dem. Rep.', 'LA', 'LAO', '418'),
(150, 'Latvia', 'LV', 'LVA', '428'),
(151, 'Lebanon', 'LB', 'LBN', '422'),
(152, 'Lesotho', 'LS', 'LSO', '426'),
(153, 'Liberia', 'LR', 'LBR', '430'),
(154, 'Libya', 'LY', 'LBY', '434'),
(155, 'Lithuania', 'LT', 'LTU', '440'),
(156, 'Luxembourg', 'LU', 'LUX', '442'),
(157, 'Madagascar', 'MG', 'MDG', '450'),
(158, 'Malawi', 'MW', 'MWI', '454'),
(159, 'Malaysia', 'MY', 'MYS', '458'),
(160, 'Maldives', 'MV', 'MDV', '462'),
(161, 'Mali', 'ML', 'MLI', '466'),
(162, 'Malta', 'MT', 'MLT', '470'),
(163, 'Marshall Isds', 'MH', 'MHL', '584'),
(164, 'Martinique', 'MQ', 'MTQ', '474'),
(165, 'Mauritania', 'MR', 'MRT', '478'),
(166, 'Mauritius', 'MU', 'MUS', '480'),
(167, 'Mayotte', 'YT', 'MYT', '175'),
(168, 'Mexico', 'MX', 'MEX', '484'),
(169, 'Mongolia', 'MN', 'MNG', '496'),
(170, 'Montenegro', 'ME', 'MNE', '499'),
(171, 'Montserrat', 'MS', 'MSR', '500'),
(172, 'Morocco', 'MA', 'MAR', '504'),
(173, 'Mozambique', 'MZ', 'MOZ', '508'),
(174, 'Myanmar', 'MM', 'MMR', '104'),
(175, 'N. Mariana Isds', 'MP', 'MNP', '580'),
(176, 'Namibia', 'NA', 'NAM', '516'),
(177, 'Nauru', 'NR', 'NRU', '520'),
(178, 'Nepal', 'NP', 'NPL', '524'),
(179, 'Neth. Antilles', 'AN', 'ANT', '530'),
(180, 'Neth. Antilles and Aruba', 'AN', 'ANT', '532'),
(181, 'Netherlands', 'NL', 'NLD', '528'),
(182, 'Neutral Zone', 'N/A', 'N/A', '536'),
(183, 'New Caledonia', 'NC', 'NCL', '540'),
(184, 'New Zealand', 'NZ', 'NZL', '554'),
(185, 'Nicaragua', 'NI', 'NIC', '558'),
(186, 'Niger', 'NE', 'NER', '562'),
(187, 'Nigeria', 'NG', 'NGA', '566'),
(188, 'Niue', 'NU', 'NIU', '570'),
(189, 'Norfolk Isds', 'NF', 'NFK', '574'),
(190, 'North America and Central America, nes', 'N/A', 'N/A', '637'),
(191, 'Northern Africa, nes', 'N/A', 'N/A', '290'),
(192, 'Norway', 'NO', 'NOR', '579'),
(193, 'Oceania, nes', 'N/A', 'N/A', '527'),
(194, 'Oman', 'OM', 'OMN', '512'),
(195, 'Other Africa, nes', 'N/A', 'N/A', '577'),
(196, 'Other Asia, nes', 'N/A', 'N/A', '490'),
(197, 'Other Europe, nes', 'N/A', 'N/A', '568'),
(198, 'Pakistan', 'PK', 'PAK', '586'),
(199, 'Palau', 'PW', 'PLW', '585'),
(200, 'Panama', 'PA', 'PAN', '591'),
(201, 'Papua New Guinea', 'PG', 'PNG', '598'),
(202, 'Paraguay', 'PY', 'PRY', '600'),
(203, 'Peninsula Malaysia', 'N/A', 'N/A', '459'),
(204, 'Peru', 'PE', 'PER', '604'),
(205, 'Philippines', 'PH', 'PHL', '608'),
(206, 'Pitcairn', 'PN', 'PCN', '612'),
(207, 'Poland', 'PL', 'POL', '616'),
(208, 'Portugal', 'PT', 'PRT', '620'),
(209, 'Qatar', 'QA', 'QAT', '634'),
(210, 'Rep. of Korea', 'KR', 'KOR', '410'),
(211, 'Rep. of Moldova', 'MD', 'MDA', '498'),
(212, 'Rest of America, nes', 'N/A', 'N/A', '636'),
(213, 'R?union', 'RE', 'REU', '638'),
(214, 'Romania', 'RO', 'ROU', '642'),
(215, 'Russian Federation', 'RU', 'RUS', '643'),
(216, 'Rwanda', 'RW', 'RWA', '646'),
(217, 'Ryukyu Isd', 'N/A', 'N/A', '647'),
(218, 'Sabah', 'N/A', 'N/A', '461'),
(219, 'Saint Barth?lemy', 'BL', 'BLM', '652'),
(220, 'Saint Helena', 'SH', 'SHN', '654'),
(221, 'Saint Kitts and Nevis', 'KN', 'KNA', '659'),
(222, 'Saint Kitts, Nevis and Anguilla', 'KN', 'KNA', '658'),
(223, 'Saint Lucia', 'LC', 'LCA', '662'),
(224, 'Saint Maarten', 'SX', 'SXM', '534'),
(225, 'Saint Pierre and Miquelon', 'PM', 'SPM', '666'),
(226, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '670'),
(227, 'Samoa', 'WS', 'WSM', '882'),
(228, 'San Marino', 'SM', 'SMR', '674'),
(229, 'Sao Tome and Principe', 'ST', 'STP', '678'),
(230, 'Sarawak', 'N/A', 'N/A', '457'),
(231, 'Saudi Arabia', 'SA', 'SAU', '682'),
(232, 'Senegal', 'SN', 'SEN', '686'),
(233, 'Serbia', 'RS', 'SRB', '688'),
(234, 'Serbia and Montenegro', 'CS', 'SCG', '891'),
(235, 'Seychelles', 'SC', 'SYC', '690'),
(236, 'Sierra Leone', 'SL', 'SLE', '694'),
(237, 'Sikkim', 'N/A', 'N/A', '698'),
(238, 'Singapore', 'SG', 'SGP', '702'),
(239, 'Slovakia', 'SK', 'SVK', '703'),
(240, 'Slovenia', 'SI', 'SVN', '705'),
(241, 'So. African Customs Union', 'ZA', 'ZAF', '711'),
(242, 'Solomon Isds', 'SB', 'SLB', '90'),
(243, 'Somalia', 'SO', 'SOM', '706'),
(244, 'South Africa', 'ZA', 'ZAF', '710'),
(245, 'South Georgia ', 'GS', 'SGS', '239'),
(246, 'South Sudan', 'SS', 'SSD', '728'),
(247, 'Spain', 'ES', 'ESP', '724'),
(248, 'Special Categories', 'N/A', 'N/A', '839'),
(249, 'Sri Lanka', 'LK', 'LKA', '144'),
(250, 'State of Palestine', 'PS', 'PSE', '275'),
(251, 'Sudan', 'SD', 'SDN', '729'),
(252, 'Suriname', 'SR', 'SUR', '740'),
(253, 'Swaziland', 'SZ', 'SWZ', '748'),
(254, 'Sweden', 'SE', 'SWE', '752'),
(255, 'Switzerland', 'CH', 'CHE', '757'),
(256, 'Syria', 'SY', 'SYR', '760'),
(257, 'Tajikistan', 'TJ', 'TJK', '762'),
(258, 'TFYR of Macedonia', 'MK', 'MKD', '807'),
(259, 'Thailand', 'TH', 'THA', '764'),
(260, 'Timor-Leste', 'TL', 'TLS', '626'),
(261, 'Togo', 'TG', 'TGO', '768'),
(262, 'Tokelau', 'TK', 'TKL', '772'),
(263, 'Tonga', 'TO', 'TON', '776'),
(264, 'Trinidad and Tobago', 'TT', 'TTO', '780'),
(265, 'Tunisia', 'TN', 'TUN', '788'),
(266, 'Turkey', 'TR', 'TUR', '792'),
(267, 'Turkmenistan', 'TM', 'TKM', '795'),
(268, 'Turks and Caicos Isds', 'TC', 'TCA', '796'),
(269, 'Tuvalu', 'TV', 'TUV', '798'),
(270, 'Uganda', 'UG', 'UGA', '800'),
(271, 'Ukraine', 'UA', 'UKR', '804'),
(272, 'United Arab Emirates', 'AE', 'ARE', '784'),
(273, 'United Kingdom', 'GB', 'GBR', '826'),
(274, 'United Rep of Tanzania', 'TZ', 'TZA', '834'),
(275, 'United States Minor Outlying Islands', 'UM', 'UMI', '581'),
(276, 'Uruguay', 'UY', 'URY', '858'),
(277, 'US Misc. Pacific Isds', 'N/A', 'N/A', '849'),
(278, 'US Virgin Isds', 'VI', 'VIR', '850'),
(279, 'USA', 'US', 'USA', '842'),
(281, 'Uzbekistan', 'UZ', 'UZB', '860'),
(282, 'Vanuatu', 'VU', 'VUT', '548'),
(283, 'Venezuela', 'VE', 'VEN', '862'),
(284, 'Viet Nam', 'VN', 'VNM', '704'),
(285, 'Wallis and Futuna Isds', 'WF', 'WLF', '876'),
(286, 'Western Asia, nes', 'N/A', 'N/A', '879'),
(287, 'Western Sahara', 'EH', 'ESH', '732'),
(288, 'World', 'WL', 'WLD', '0'),
(289, 'Yemen', 'YE', 'YEM', '887'),
(290, 'Zambia', 'ZM', 'ZMB', '894'),
(291, 'Zimbabwe', 'ZW', 'ZWE', '716');

-- --------------------------------------------------------

--
-- Table structure for table `dptypesetup`
--

CREATE TABLE `dptypesetup` (
  `DP_ID` int(11) NOT NULL,
  `DPType` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dptypesetup`
--

INSERT INTO `dptypesetup` (`DP_ID`, `DPType`) VALUES
(1, 'DOC'),
(2, 'Parcel'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `exchangerate`
--

CREATE TABLE `exchangerate` (
  `id` int(34) NOT NULL,
  `Start_Date` date DEFAULT NULL,
  `End_Date` date DEFAULT NULL,
  `exchangerate` int(43) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exchangerate`
--

INSERT INTO `exchangerate` (`id`, `Start_Date`, `End_Date`, `exchangerate`) VALUES
(1, '2018-12-12', '2018-12-01', 230);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `LocationID` int(5) NOT NULL COMMENT 'LocationID',
  `LocationName` varchar(100) NOT NULL COMMENT 'Name of Location',
  `LocationShortName` varchar(10) NOT NULL COMMENT 'ShortName for Location',
  `StateID` int(5) NOT NULL COMMENT 'ProvanceID',
  `GSTPercent` decimal(10,0) NOT NULL COMMENT 'GST Percentage Amt from State Table',
  `ContactPerson` varchar(70) DEFAULT NULL COMMENT 'Contact Person at Location',
  `ContactNo` varchar(15) DEFAULT NULL COMMENT 'Contact Number for Location',
  `EmailID` varchar(30) DEFAULT NULL COMMENT 'Email ID',
  `Detail` varchar(300) DEFAULT NULL COMMENT 'Further any Detail'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`LocationID`, `LocationName`, `LocationShortName`, `StateID`, `GSTPercent`, `ContactPerson`, `ContactNo`, `EmailID`, `Detail`) VALUES
(1, 'Board Office  (Karachi)', 'BOK', 3, '15', '27', '02223124', 'asif@gmail.com', 'Branch Manager'),
(2, 'Gulshan', 'G1', 1, '5', '27', '02223124212', 'Saif@gmail.com', 'Lahore Branch Manager'),
(3, 'Joher Town (LHR)', 'JTL', 2, '18', '28', '02223124212', 'Kashif@gmail.com', 'Lahore Branch Manager');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `staff_name` varchar(100) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(10) DEFAULT NULL,
  `cnic` varchar(10) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `education` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `salary` int(6) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `staff_name`, `middle_name`, `last_name`, `email`, `phone`, `address`, `city`, `cnic`, `gender`, `dob`, `education`, `designation`, `salary`, `image`) VALUES
(35, 'ali shah', 'shah', 'ali', 'ali@gmail.com', 121312312, 'fb area', 'karachi', '21312-1231', 'Male', '2018-11-14', 'phd', 'job', 12000, '1543223152index.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `stategst`
--

CREATE TABLE `stategst` (
  `StateID` int(5) NOT NULL COMMENT 'StateID',
  `StateName` varchar(30) NOT NULL COMMENT 'Name of State/Provance',
  `GST` decimal(5,0) NOT NULL COMMENT '% of GST Applicable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stategst`
--

INSERT INTO `stategst` (`StateID`, `StateName`, `GST`) VALUES
(1, 'balochistan', '5'),
(2, 'Punjab', '18'),
(3, 'Sindh', '15');

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(222) NOT NULL,
  `Start_Date` date DEFAULT NULL,
  `End_Date` date DEFAULT NULL,
  `StateID` int(222) DEFAULT NULL,
  `GSTPercent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `Start_Date`, `End_Date`, `StateID`, `GSTPercent`) VALUES
(18, '2018-11-01', '2018-11-09', 1, 12),
(19, '2018-11-23', '2018-11-13', 2, 16),
(20, '2018-11-23', '2018-11-30', 3, 23),
(21, '2018-11-01', '2018-11-02', 0, 23),
(23, '2018-11-01', '2018-11-02', 48, 23),
(24, '2018-11-02', '2018-11-02', 49, 23);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_last_login`
--

CREATE TABLE `tbl_last_login` (
  `id` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `sessionData` varchar(2048) NOT NULL,
  `machineIp` varchar(1024) NOT NULL,
  `userAgent` varchar(128) NOT NULL,
  `agentString` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `createdDtm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_last_login`
--

INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(1, 13, '{\"role\":\"1\",\"roleText\":\"EXpress\",\"name\":\"Admin\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 8.1', '2018-11-16 18:05:50'),
(2, 13, '{\"role\":\"1\",\"roleText\":\"EXpress\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 8.1', '2018-11-16 18:15:30'),
(3, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 8.1', '2018-11-16 18:16:27'),
(4, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.102', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Windows 8.1', '2018-11-17 17:48:52'),
(5, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Firefox 63.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0', 'Windows 10', '2018-11-20 13:34:10'),
(6, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Firefox 63.0', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0', 'Windows 10', '2018-11-22 10:32:35'),
(7, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-23 12:57:58'),
(8, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-23 15:31:19'),
(9, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-24 11:11:57'),
(10, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-24 13:02:50'),
(11, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-24 18:30:33'),
(12, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-25 15:34:17'),
(13, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-25 19:41:57'),
(14, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Firefox 62.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0', 'Windows 10', '2018-11-26 10:37:31'),
(15, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Firefox 63.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0', 'Windows 10', '2018-11-26 23:26:24'),
(16, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Firefox 63.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0', 'Windows 10', '2018-11-27 10:20:14'),
(17, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-27 13:44:52'),
(18, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-27 18:01:35'),
(19, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Firefox 63.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0', 'Windows 10', '2018-11-28 10:39:44'),
(20, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-28 12:54:16'),
(21, 27, '{\"role\":\"8\",\"roleText\":\"Board Office(Branch Manager)\",\"name\":\"Asif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-28 13:31:19'),
(22, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-28 13:32:06'),
(23, 27, '{\"role\":\"3\",\"roleText\":\"Branch Manager\",\"name\":\"Asif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-28 13:57:54'),
(24, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-28 13:59:48'),
(25, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-28 14:58:08'),
(26, 28, '{\"role\":\"3\",\"roleText\":\"Branch Manager\",\"name\":\"Kashif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-28 14:58:59'),
(27, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-28 14:59:28'),
(28, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-29 13:43:10'),
(29, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-11-30 10:25:11'),
(30, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 10:41:40'),
(31, 28, '{\"role\":\"3\",\"roleText\":\"Branch Manager\",\"name\":\"Kashif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:40:17'),
(32, 28, '{\"role\":\"3\",\"roleText\":\"Branch Manager\",\"name\":\"Kashif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:41:47'),
(33, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:42:44'),
(34, 28, '{\"role\":\"3\",\"roleText\":\"Branch Manager\",\"name\":\"Kashif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:42:50'),
(35, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:43:49'),
(36, 28, '{\"role\":\"3\",\"roleText\":\"BranchManager\",\"name\":\"Kashif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:45:11'),
(37, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:48:45'),
(38, 28, '{\"role\":\"3\",\"roleText\":\"BranchManager\",\"name\":\"Kashif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:51:54'),
(39, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:52:25'),
(40, 28, '{\"role\":\"3\",\"roleText\":\"BranchManager\",\"name\":\"Kashif\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:53:36'),
(41, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-01 11:58:07'),
(42, 13, '{\"role\":\"1\",\"roleText\":\"ICL Express\",\"name\":\"Kamran Iqbal\"}', '::1', 'Chrome 70.0.3538.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 'Windows 10', '2018-12-04 10:53:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset_password`
--

CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` bigint(20) NOT NULL DEFAULT '1',
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_reset_password`
--

INSERT INTO `tbl_reset_password` (`id`, `email`, `activation_id`, `agent`, `client_ip`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(1, 'admin@example.com', 'hVJbqGLTdn0ryag', 'Chrome 69.0.3497.100', '::1', 0, 1, '2018-10-02 18:18:56', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
(1, 'ICL Express'),
(2, 'Manager'),
(3, 'Branch_Manager');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `roleId`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(13, 'KI@Interflow.com.pk', '$2y$10$NrmwkXQhclGMSs0l8gZIAumOnnFxwcpKhCdN5FHvsxp1YXwQjiq82', 'Kamran Iqbal', '0333222221', 1, 0, 1, '2018-10-02 11:53:31', 13, '2018-11-16 14:17:07'),
(15, 'JK@Interflow.com.pk', '$2y$10$a/DiXlblQsColAc9PGGZCOaDB2szWITNKQnR.5VobZ3cvs.e0mQNS', 'Junaid Khan', '0333221171', 2, 1, 14, '2018-10-02 18:51:07', 13, '2018-11-02 20:17:10'),
(26, 'ki@terflow.com.pk', '$2y$10$o/xZPW8kQA2/.t99yj1XQO4lqIl2l9C8tM4FuA4HRkkW29SUbEFqO', 'Ali', '3243242342', 2, 0, 13, '2018-11-25 15:56:00', NULL, NULL),
(27, 'asif@gmail.com', '$2y$10$8K0t6qZzehDPVw/M9is2UeKwsLNgWElaf2sEmfuipcguHBo771SC.', 'Asif', '3243242342', 3, 0, 13, '2018-11-28 09:31:01', NULL, NULL),
(28, 'kashif@gmail.com', '$2y$10$KJLCfuh8f2.pgcDaR9vM2OJagqhfumoMmXSeFzWBhVE0I2gPl6pN6', 'Kashif', '3243242342', 3, 0, 13, '2018-11-28 09:36:57', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `awbform`
--
ALTER TABLE `awbform`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `companyprofile`
--
ALTER TABLE `companyprofile`
  ADD PRIMARY KEY (`companyId`);

--
-- Indexes for table `dptypesetup`
--
ALTER TABLE `dptypesetup`
  ADD PRIMARY KEY (`DP_ID`);

--
-- Indexes for table `exchangerate`
--
ALTER TABLE `exchangerate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`LocationID`,`LocationName`,`LocationShortName`),
  ADD KEY `GetStateID` (`StateID`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `stategst`
--
ALTER TABLE `stategst`
  ADD PRIMARY KEY (`StateID`,`StateName`),
  ADD UNIQUE KEY `UC_StateName` (`StateName`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_last_login`
--
ALTER TABLE `tbl_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `awbform`
--
ALTER TABLE `awbform`
  MODIFY `id` int(23) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `dptypesetup`
--
ALTER TABLE `dptypesetup`
  MODIFY `DP_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `exchangerate`
--
ALTER TABLE `exchangerate`
  MODIFY `id` int(34) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `LocationID` int(5) NOT NULL AUTO_INCREMENT COMMENT 'LocationID', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `stategst`
--
ALTER TABLE `stategst`
  MODIFY `StateID` int(5) NOT NULL AUTO_INCREMENT COMMENT 'StateID', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(222) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_last_login`
--
ALTER TABLE `tbl_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `location`
--
ALTER TABLE `location`
  ADD CONSTRAINT `GetStateID` FOREIGN KEY (`StateID`) REFERENCES `stategst` (`StateID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
