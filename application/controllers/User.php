<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'libraries/BaseController.php';
// APPPATH . 'libraries/pdf.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('Main_Model');
        $this->load->model('Insert');
        $this->isLoggedIn();   
        $this->load->library('session');
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'CodeInsect : Dashboard';


        

     
      
      

       
      // print_r($this->session->flashdata('my_super_array'));
        

        
       /* $this->load->model('Insert');

  $query['students']  =    $this->Insert->getway()->num_rows();
  $query['rate']      =      $this->Insert->rate()->num_rows();
  $query['stategst']  =   $this->Insert->mystate()->num_rows();
  $query['tbl_users'] = $this->Insert->tbl_users()->num_rows();*/
      

  $this->loadViews("dashboard",$this->global);

    }

    public function dashboard1(){

      $this->global['pageTitle'] = 'CodeInsect : Dashboard';
        
        
        $this->load->model('Insert');

  $query['students']=$this->Insert->getway()->num_rows();
  $query['rate']=$this->Insert->rate()->num_rows();
  $query['stategst']=$this->Insert->mystate()->num_rows();
  $query['tbl_users']=$this->Insert->tbl_users()->num_rows();

      $this->loadViews("dashboard1",$this->global,$query);

      

    }


    public function dashboard2(){


      //$data['userid'] = $this->session->('my_super_array');
       $data['userid'] = $this->session->userdata('my_super_array');
//exit();
      $this->loadViews("dashboard2",$this->global,$data);

    }



public function edlo()
{

  $location_id = $_GET['id'];

  $query = $this->db->query("SELECT * FROM location where LocationID =  ' $location_id ' ");
  $data['data'] = $query->row();

$query = $this->db->query(" SELECT * FROM stategst ");
  $data['state'] = $query->result();

  $query = $this->db->query(" SELECT * FROM tbl_users ");
  $data['user'] = $query->result();
  

  $this->loadViews("editlocation",$this->global,$data);
}





  public function getvalue()
  {
   if ($this->input->post('StateID'))
    {
   echo $this->Insert->getvalue($this->input->post('StateID'));
   }
  }

        public function program(){
            $data['all_pro'] = $this->Main_Model->show_pro();
            $this->loadViews("program", $this->global, $data, NULL);
        }

        public function addprogram(){

            
            $this->loadViews("addprogramform", $this->global);
        } 

         public function awbform(){
          
                $data['userid'] = $this->session->userdata('my_super_array');
                  $this->load->model("Insert");
                  $data['record']=$this->Insert->getarea();
                  $data['rec']=$this->Insert-> getbox();
                  $data['count']=$this->Insert->getcountry();

                  $this->loadViews("awbform",$this->global,$data);
 
            } 

 public function staff_edit()
 {
  $this->load->model("Insert");
   $LocationID = $this->input->post('name');
   $result=$this->Insert->stategstedit($LocationID);
   if(!empty($result))
   {
    $data['result']=$result;
     $this->loadViews("staff_edit",$this->global);


   }
   else{
    $this->loadViews("staff_edit",$this->global);
}


 }














public function showprice(){
  
      $id=$_POST['ff'];
      //print_r($id);
      //exit;
      $this->db->select('*');
      $this->db->from('stategst');
      $this->db->where('StateID',$id);
      $query = $this->db->get();
      $data = $query->result_array();
      
     
   
     echo $this->view($data);
    
     //echo json_encode($ui);
  }

    public function view($data){
      foreach ($data as $row) {
        
      
       ?>
  

    

   
                            

    <div class="form-group">

    <label style="color:black;">GST Percent</label>
<input type="text" name="GSTPercent" placeholder="Enter GST" value="<?php echo $row['GSTP'] ?>"  id="GSTPercent" class="form-control" disabled >

        </div>
                            
                        




<?php }
     } 
























































public function updatelocation(){


         echo $location_id = $this->input->post("location_id");
         echo $port=$this->input->post("port");
         echo $LocationShortName=$this->input->post("LocationShortName");
        // echo $StateID=$this->input->post("StateID");
        // echo $GSTPercent=$this->input->post("GSTPercent");
         echo $ContactPerson=$this->input->post("ContactPerson");
         echo $ContactNo=$this->input->post("ContactNo");
         echo $EmailID=$this->input->post("EmailID");
         echo $Detail=$this->input->post("Detail");

         $data = 
         [ 'LocationName'=>$port,
           'LocationShortName'=>$LocationShortName,
          
        
           'ContactPerson'=>$ContactPerson,
           'ContactNo'=>$ContactNo,
           'EmailID'=>$EmailID,
           'Detail'=> $Detail ];



        $this->db->where('LocationID',$location_id);
        $result=$this->db->update('location',$data);
        
        redirect(base_url('location'));  


}


 public function insertstate()
        {

$this->load->library('form_validation');
$this->form_validation->set_rules('port','locationname','required|is_unique[location.LocationName]');


if ($this->form_validation->run()==false)
{
  $this->load->model("Insert");
$fatch["Record"]=$this->Insert->getgst();
$fatch['read']=$this->Insert->getdataflow();
$fatch['message']="Select Another Name";
$this->loadViews("locate",$this->global,$fatch);
}

else {








         $port=$this->input->post("port");
         $LocationShortName=$this->input->post("LocationShortName");
         $StateID=$this->input->post("StateID");
         $GSTPercent=$this->input->post("GSTPercent");
         $ContactPerson=$this->input->post("ContactPerson");
         $ContactNo=$this->input->post("ContactNo");
         $EmailID=$this->input->post("EmailID");
         $Detail=$this->input->post("Detail");

         $data=['LocationName'=>$port,
         'LocationShortName'=>$LocationShortName,
         'StateID'=>$StateID,
         'GSTPercent'=>$GSTPercent,
         'ContactPerson'=>$ContactPerson,
         'ContactNo'=>$ContactNo,
         'EmailID'=>$EmailID,
         'Detail'=> $Detail];

         $this->load->model("Insert");
         $this->Insert->state('location',$data);

        redirect(base_url('location'));  

 }
        }



         public function insertstaff()
        {


$this->load->library('form_validation');

$this->form_validation->set_rules('staff_name','staff_name','required');
$this->form_validation->set_rules('middle_name','middle_name','required');
$this->form_validation->set_rules('last_name','last_name','required');
$this->form_validation->set_rules('email','email','required');
$this->form_validation->set_rules('phone','phone','required');
$this->form_validation->set_rules('address','address','required');
$this->form_validation->set_rules('city','city','required');
$this->form_validation->set_rules('cnic','cnic','required');
$this->form_validation->set_rules('gender','gender','required');
$this->form_validation->set_rules('dob','dob','required');
$this->form_validation->set_rules('education','education','required');
$this->form_validation->set_rules('designation','designation','required');
$this->form_validation->set_rules('salary','salary','required');

 

if ($this->form_validation->run()==false)
{

$data['message']="field is required";
$this->loadViews("addstaff",$this->global,$data);
}

else {



         $target_dir = "upload/";
         $target_file = $target_dir.time().basename($_FILES["image"]["name"]);
         $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
         $imgName = time().basename($_FILES["image"]["name"]);
         move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
         $staff_name=$this->input->post("staff_name");
         $middle_name=$this->input->post("middle_name");
         $last_name=$this->input->post("last_name");
         $email=$this->input->post("email");
         $phone=$this->input->post("phone");
         $address=$this->input->post("address");
         $city=$this->input->post("city");
         $cnic=$this->input->post("cnic");
         $gender=$this->input->post("gender");
         $dob=$this->input->post("dob");
         $education=$this->input->post("education");
         $designation=$this->input->post("designation");
         $salary=$this->input->post("salary");
        
        
         $data=[
         'staff_name'=>$staff_name,
         'middle_name'=>$middle_name,
         'last_name'=>$last_name,
         'email'=>$email,
         'phone'=>$phone,
         'address'=>$address,
         'city'=>$city,
         'cnic'=>$cnic,
         'gender'=>$gender,
         'dob'=>$dob,
         'education'=>$education,
         'designation'=>$designation,
         'salary'=>$salary,
         'image'=>$imgName
         
         ];


       $this->load->model("Insert");
       $this->Insert->insertstaff('staff',$data);

        redirect(base_url('staff'));  
}
}




      

 
        
        public function addstaff()
        {
          $this->loadViews("addtaff",$this->global);
        }


public function staff()
{
  $this->load->model("Insert");
  $data['fatch']=$this->Insert->getstaff();
 $this->loadViews("staff",$this->global,$data);
}


public function addNewstaff()
{
 $this->loadViews("addNewstaff",$this->global);
}




























  public function bracchmanager()
       {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('AWBID','AWBID','required|is_unique[awbform.AWBID]');
            $this->form_validation->set_rules('DepositSlip','DepositSlip','required|min_length[7]');





 

          if ($this->form_validation->run()==false)
        {

              $this->load->model("Insert");
              $data['record']=$this->Insert->getarea();
              $data['rec']=$this->Insert-> getbox();
              $data['count']=$this->Insert->getcountry();
              $data['sent']="Atleast 7 character";
              $data['roll']="AWBID is unique";

              $this->loadViews("awbformdo",$this->global,$data);


          }

            else
          {





                $cdate=$this->input->post('cdate');

                $AWBID=$this->input->post('AWBID');
                $selectlocation=$this->input->post('selectlocation');
                $selectDoxParcel=$this->input->post('selectDoxParcel');
                $selectDestination=$this->input->post('selectDestination');
                $Weight=$this->input->post('Weight');
                $Tax=$this->input->post('Tax');
                $actualprice=$this->input->post('actualprice');
                


                $SellingPrice=$this->input->post('SellingPrice');
                $DepositSlip=$this->input->post('DepositSlip');

                $DepositDate=$this->input->post('DepositDate');
                $BankReference=$this->input->post('BankReference');

                $userid=$this->input->post('userid');

                $locationid=$this->input->post('location_id');


             

                $dicount = $actualprice-$SellingPrice;
                $dicountpersentage = ($actualprice-$SellingPrice)/$actualprice*100;


    $data=[
      'date'=> $cdate,
      'AWBID'=>$AWBID,
      'LocationID'=>$selectlocation,
      'Tax'=>$Tax,
      'ActualPrice'=>$actualprice,
      
      'Discont'=>$dicount,
      'DP_ID'=>$selectDoxParcel,
      'CountryID'=>$selectDestination,
      'Weight'=>$Weight,
      'dicountper'=>$dicountpersentage,
      'SellingPrice'=>$SellingPrice,
      'DepositSlip'=>$DepositSlip,
      'DepositDate'=> $DepositDate,
      'user_id'=>$userid,
      'BankReference'=>$BankReference,
      'location_id' => $locationid

];
                
                     
                 $this->db->insert('awbform',$data);

                 //$this->load->model('Insert');
                 //$this->Insert->form('awbform',$data);

                 redirect(base_url().'dashboard2');




}



                     
                
       }




public function Branch_manager_form(){
   $data['userid'] = $this->session->userdata('my_super_array');
                           $location_id = $this->session->userdata('Locationid');
   
                  $this->load->model("Insert");
                  $data['record']=$this->Insert->getarea1($location_id);
                  $data['rec']=$this->Insert-> getbox();
                  $data['count']=$this->Insert->getcountry();

                  $this->loadViews("Branch_manager_form",$this->global,$data);
}































       public function show()
       {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('AWBID','AWBID','required|is_unique[awbform.AWBID]');
            $this->form_validation->set_rules('DepositSlip','DepositSlip','required|min_length[7]');





 

          if ($this->form_validation->run()==false)
        {

              $this->load->model("Insert");
              $data['record']=$this->Insert->getarea();
              $data['rec']=$this->Insert-> getbox();
              $data['count']=$this->Insert->getcountry();
              $data['sent']="Atleast 7 character";
              $data['roll']="AWBID is unique";

              $this->loadViews("awbformdo",$this->global,$data);


          }

            else
          {

                $cdate=$this->input->post('cdate');

                $AWBID=$this->input->post('AWBID');
                $selectlocation=$this->input->post('selectlocation');
                $selectDoxParcel=$this->input->post('selectDoxParcel');
                $selectDestination=$this->input->post('selectDestination');
                $Weight=$this->input->post('Weight');
                $Tax=$this->input->post('Tax');
                $actualprice=$this->input->post('actualprice');
                


                $SellingPrice=$this->input->post('SellingPrice');
                $DepositSlip=$this->input->post('DepositSlip');

                $DepositDate=$this->input->post('DepositDate');
                $BankReference=$this->input->post('BankReference');

                $userid=$this->input->post('userid');


             

                $dicount = $actualprice-$SellingPrice;
                $dicountpersentage = ($actualprice-$SellingPrice)/$actualprice*100;


    $data=[
      'date'=> $cdate,
      'AWBID'=>$AWBID,
      'LocationID'=>$selectlocation,
      'Tax'=>$Tax,
      'ActualPrice'=>$actualprice,
      
      'Discont'=>$dicount,
      'DP_ID'=>$selectDoxParcel,
      'CountryID'=>$selectDestination,
      'Weight'=>$Weight,
      'dicountper'=>$dicountpersentage,
      'SellingPrice'=>$SellingPrice,
      'DepositSlip'=>$DepositSlip,
      'DepositDate'=> $DepositDate,
      'user_id'=>$userid,
      'BankReference'=>$BankReference
];
                
                     
                 

                 $this->load->model('Insert');
                 $this->Insert->form('awbform',$data);

                 redirect(base_url().'dashboard');




}



                     
                
       }




        public function tax(){


           $this->load->model("Insert");
           $data['record']= $this->Insert->getfind();
           $this->loadViews("tax",$this->global,$data);

         

 }

  public function addtax(){


$this->load->model("Insert");
$data['record']=$this->Insert->findgst();
$this->loadViews("addtax",$this->global,$data);
 }



 // public function tax_edited()
 //  {
 //     $this->load->model("Insert");
 //   $id=$this->input->post('id');
 //   $result=$this->Insert->load_editted($id);
 //   if(!empty($result))
 //   {
 //    $data['result']=$result;
 //     $this->loadViews("load_editted_view",$this->global,$data);


 //   }
 //   else{
 //    $this->loadViews("load_editted_view",$this->global,NULL);


 //   }

  

 //  }


  // public function taxupdate()
  // {
  //   $this->load->model('Insert');
  //   $id=$this->input->post('id');
  //   $end=$this->input->post('end');
  //   $start=$this->input->post('start');
  //   $tax=$this->input->post('tax');
  //   $sellist=$this->input->post('sellist');
  //   $result=$this->Insert->load_edit_tax($id,$end,$start,$tax,$sellist);
  //  if($result)
  //  {
  //   //echo $result;
  //  redirect(base_url().'tax');


  //  }
  //  else{
  //   //echo $result;
  //  redirect(base_url().'tax');


  //  }



  // }

 

public function editgst()
{
      $this->load->model("Insert");
   $StateID=$this->input->post('id');
   $result=$this->Insert->stategstedit($StateID);
   if(!empty($result))
   {
    $data['result']=$result;
     $this->loadViews("editgst",$this->global,$data);


   }
   else{
    $this->loadViews("editgst",$this->global,NULL);
}
}



public function stategstupdated()
  {
    
    $this->load->model('Insert');
    $StateID=$this->input->post('id');
    $statename=$this->input->post('statename');
    $gst=$this->input->post('gst');

    $result=$this->Insert->updatedlocation($StateID,$statename,$gst);

   if($result)
   {
   
   redirect(base_url().'stategst');


   }
   else{
   
   redirect(base_url().'stategst');


   }



  }

 public function awbformupdated()
   {
     $this->load->model('Insert');

  echo "</br>";
     echo " StateID= ". $StateID=$this->input->post('id');
     echo "</br>";
     echo " StateID=cdate ". $cdate=$this->input->post('cdate');
     echo "</br>";
     echo " AWBID= ".$AWBID=$this->input->post('AWBID');
     echo "</br>";
     echo " selectDoxParcel= ".$selectDoxParcel=$this->input->post('selectDoxParcel');
     echo "</br>";
     echo " selectlocation= ".$selectlocation=$this->input->post('selectlocation');
     echo "</br>";
     echo " Tax= ".$Tax=$this->input->post('Tax');
     echo "</br>";
     echo " selectDestination= ".$selectDestination=$this->input->post('selectDestination');
     echo "</br>";
     echo " Weight= ".$Weight=$this->input->post('Weight');
     echo "</br>";
     echo " actualprice= ".$actualprice=$this->input->post('actualprice');
     echo "</br>";
     echo " SellingPrice= ".$SellingPrice=$this->input->post('SellingPrice');
     echo "</br>";
     echo " DepositSlip= ".$DepositSlip=$this->input->post('DepositSlip');
     echo "</br>";
     echo " DepositDate= ".$DepositDate=$this->input->post('DepositDate');
     echo "</br>";
     echo " BankReference= ".$BankReference=$this->input->post('BankReference');


     /*$data=array(


      'date'=>$cdate,
      'AWBID'=>$AWBID,
      'DP_ID'=>$selectlocation,
      'CountryID'=>$selectDestination,
      'Weight'=>$Weight,
      'LocationID'=>$selectlocation,
      'Tax'=>$Tax,
      'ActualPrice'=>$actualprice,
      
      'SellingPrice'=>$SellingPrice,
      'DepositSlip'=>$DepositSlip,
      'DepositDate'=>$DepositDate,
      'BankReference'=>$BankReference



    );

*/    /* $this->db->where('AWBID',$AWBID);
    $result=$this->db->update('awbform',$data);

    if ($result) {
      echo "YAS";
    }else{
      echo "NO";
    }*/


     //exit();


     $result=$this->Insert->updated_aw_form($StateID,$cdate,$AWBID,$selectlocation,$Tax,$electDoxParcel,$selectDestination,$Weight,$actualprice,$SellingPrice,$DepositSlip,$DepositDate,$BankReference);

    if($result)
    {
   
    redirect(base_url().'dashboard');


    }
    else{
   
   redirect(base_url().'Search');


   }


  }

 




  public function exupdate()
  {
    $this->load->model('Insert');
    $id=$this->input->post('id');
    $end=$this->input->post('end');
    $start=$this->input->post('start');
    $tax=$this->input->post('tax');
    
$result=$this->Insert->load_edit_exchangerate($id,$end,$start,$tax);

   if($result)
   {
    //echo $result;
   redirect(base_url().'exchangerate');


   }
   else{
    //echo $result;
   redirect(base_url().'exchangerate');


   }



  }
       

       public function taxinsert()
        {
          $start=$this->input->post("start");
         $end=$this->input->post("end");
         $taxy=$this->input->post("taxy");
         $sellist=$this->input->post("sellist");

         $data=['Start_Date'=>$start,
         'End_Date'=>$end,
         'StateID'=>$sellist,
         'GSTPercent'=>$taxy
         
       ];

       $this->load->model("Insert");
       $this->Insert->ingst('tax',$data);

        redirect(base_url('tax'));  

 
        }






      public function location(){

$this->load->model("Insert");
$fatch["Record"]=$this->Insert->getstate();
$this->loadViews("location",$this->global,$fatch);




 }

     public function addlocation()
     {
$this->load->model("Insert");
$fatch["Record"]=$this->Insert->getgst();
$fatch['read']=$this->Insert->getdataflow();
$this->loadViews("addlocation",$this->global,$fatch);
}




public function getgst()
{
$this->load->model('Insert');
$data['field']=$this->Insert->getgst();
$this->loadViews("stategst",$this->global,$data);
}

public function addgst()
{

 $this->loadViews("addgst",$this->global);
}

 public function insert_gst()
{


$this->load->library('form_validation');
$this->form_validation->set_rules('statename','statename','required|is_unique[stategst.statename]');

if ($this->form_validation->run()==false)
{
$data['message']="Select Anothor State Name";

$this->loadViews("view",$this->global,$data);
}



else{

         $statename=$this->input->post("statename");
        $gst=$this->input->post("gst");
        $data=['StateName'=>$statename,'GST'=>$gst];
            $this->load->model("Insert");
            $this->Insert->insert_gst("stategst",$data);

            redirect(base_url().'stategst');
            

}

        


   }







 

  public function edt()
  {
     
     $this->load->model("Insert");
     $id=$this->input->post('id');
     $result=$this->Insert->editted($id);

   if(!empty($result))
   {
    $data['result']=$result;
     $this->loadViews("edt",$this->global,$data);


   }
   else{
    $this->loadViews("edt",$this->global,NULL);


   }

 
      

}

 public function Search_edit()
  {
     
   $this->load->model("Insert");
   $id = $this->input->get('id');
   $result = $this->Insert->Search_edit($id);
   if(!empty($result))
   {
    $data['id'] = $id;
    $data['count']=$this->Insert->getcountry();
    $data['result']=$result;
     $this->loadViews("awbform_edit",$this->global,$data);
   }
   else{
    $this->loadViews("awbform_edit",$this->global,NULL);

   }

 

}




public function Search_update()
  
     
   {
    $this->load->model('Insert');
    $id=$this->input->post('AWBID');
    $cdate=$this->input->post('start');
    $AWBID=$this->input->post('awbid');
    $sellist=$this->input->post('sellist');
    $Tax=$this->input->post('tax');
    $ActualPrice=$this->input->post('actualprice');
    $cashcollected=$this->input->post('cashcollected');
    
$result=$this->Insert->Search_update($cdate, $AWBID,$sellist,$Tax,$ActualPrice,$cashcollected);

   if($result)
   {
    //echo $result;
   redirect(base_url().'user/Search');


   }
   else{
    //echo $result;
   redirect(base_url().'user/Search');


   }


 

}


public function exrate()
  {
     
    $this->load->model("Insert");
   $id=$this->input->post('id');
   $result=$this->Insert->load($id);
   if(!empty($result))
   {
    $data['result']=$result;
     $this->loadViews("exrate",$this->global,$data);


   }
   else{
    $this->loadViews("exrate",$this->global,NULL);


   }
    


  
 


   }

 


  public function locationupdate()
  {
    $this->load->model('Insert');
    $id=$this->input->post('id');
    $tax=$this->input->post('tax');
    $sellist=$this->input->post('sellist');
    $result=$this->Insert->load_edit_location($id,$tax,$sellist);
   if($result)
   {
    //echo $result;
   redirect(base_url().'user/location');


   }
   else{
    //echo $result;
   redirect(base_url().'user/location');


   }



  }

public function taxdelete()
 {
    $id=$this->uri->segment('3');
  $query=$this->db->query("delete  from tax where id=".$id);

    
if($query == TRUE)
        {
           
            redirect(base_url('tax'));    
        }
     
        
     
 }




public function staffdel()
 {
    $id=$this->uri->segment('3');
  $query=$this->db->query("delete  from staff where staff_id=".$id);

    
if($query == TRUE)
        {
           
            redirect(base_url('staff'));    
        }
     
        
     
 }


 public function deletestate()
 {
       $id=$this->uri->segment('3');
  $query=$this->db->query("delete from location where LocationID=".$id);

    
if($query == TRUE)
        {
           
            redirect(base_url('location'));    
        }
     
 }


 public function stategstdelete()
 {
    $id=$this->uri->segment('3');
  $query=$this->db->query("delete  from stategst where StateID=".$id);

    
if($query == TRUE)
        {
           
            redirect(base_url('stategst'));    
        }
     
        
     
 }
        

// public function delete()
//  {
//     $id=$this->uri->segment('3');
//   $query=$this->db->query("delete  from location where id=".$id);

    
// if($query == TRUE)
//         {
           
//             redirect(base_url('location'));    
//         }
     
        
     
//  }
 

 public function sdel()
 {
    $id = $this->uri->segment('3');
  $query =$this->db->query("delete from awbform where AWBID=".$id);

    
if($query == TRUE)
        {
           
            redirect(base_url('Search'));    
        }
     
        
     
 }
  public function exdelete()
 {
    $id=$this->uri->segment('3');
  $query=$this->db->query("delete  from exchangerate where id=".$id);

    
if($query == TRUE)
        {
           
            redirect(base_url('exchangerate'));    
        }
     
        
     
 }

  public function insertlocation()
 {
   $add = $this->input->Post("port");
   $get = $this->input->Post("get");
   $dataa=['name'=> $add,'pname'=> $get];

   $this->load->model("Insert");

$found=$this->Insert->Location('location',$dataa);

  if($found > 0)
  {
      

      $this->session->set_flashdata('success', 'New User created successfully');
                

  }
               

             
                
               
                
          


 redirect(base_url().'location');


 }



public function exchangerate()
{
     $this->load->model("Insert");
  $form['come']=$this->Insert->getexchange();

   

   $this->loadViews("exchangerate",$this->global,$form); 

} 



public function addexchange()
{
     $this->load->model("Insert");
     $form['come']=$this->Insert->getexchange();
     $this->loadViews("addexchange",$this->global,$form); 
} 

 public function insert_exchange()
        {


          $sname = $this->input->post("sname");
          $ename = $this->input->post("ename");
          $ername = $this->input->post("ername");
         

            $dat = ['Start_Date'=>$sname,'End_Date'=>$ename,'exchangerate'=>$ername];


            $this->load->model("Insert");
            $this->Insert->exchangerate("exchangerate",$dat);

            redirect(base_url().'exchangerate');
        }

  public function brach_manager()
    {
         $id = $_GET['User'];
          //echo $id = $this->uri->segment('3');

          $data['userid'] = $id;

         



        $this->load->model('Insert');
        $data['read']=$this->Insert->branch_manager($id);
        $this->loadViews("branch_manager", $this->global,$data);
        
    } 
  
  public function Search()
    {

        $data['userid'] = $_GET['User'];

        $this->load->model('Insert');
        $data['read']=$this->Insert->record();
        $this->loadViews("Search", $this->global,$data);
        
    } 

 

 public function report()
 {

 $this->load->model("Insert");

 $data['repot']=$this->Insert->getdeal();

 $this->loadViews("report",$this->global,$data);

}

   public function Detail_report()
 {

  $this->load->model("Insert");

  $data['repot'] = $this->Insert->getdeal();

  $this->loadViews("Detail_report",$this->global,$data);

}


public function location_report()
 {

 $this->load->model("Insert");

 $data['repot']=$this->Insert->getdeal();

$this->loadViews("locationreport",$this->global,$data);

}

public function location_pdf_report()
{
  $date = $this->input->post('date');
 

  $result=$this->Insert->location_get_report_data($date);
  
  $data['result']=$result;


   $this->load->library('pdf');
   $data['sent']=$this->Insert->companyprofile();
   $this->pdf->load_view('reports_area/location_report',$data);





  
}


public function details_pdf_report()
{
  $location=$this->input->post('location');
  $start=$this->input->post('start');
  $end=$this->input->post('end');
  $result=$this->Insert->details_get_report_data($location,$start,$end);
  
  $data['result'] = $result;  


   $this->load->library('pdf');
   $data['sent']=$this->Insert->companyprofile();
   $this->pdf->load_view('reports_area/details_report',$data);





  
}

public function pdf_report()
{
  $location=$this->input->post('location');
  $start=$this->input->post('start');
  $end=$this->input->post('end');
  $result=$this->Insert->get_report_data($location,$start,$end);
  
  $data['result']=$result;


   $this->load->library('pdf');
   $data['sent']=$this->Insert->companyprofile();
$this->pdf->load_view('reports_area/report',$data);





  

}


public function viewallreport()
{
 
  $this->load->model("Insert");
  $result=$this->Insert->getallreport();
  $data['result']=$result;
  $this->load->library('pdf');
  $data['sent']=$this->Insert->companyprofile();
  $this->pdf->load_view('reports_area/viewallreport',$data); 

}




public function ShipmentDetailsreport()
{

  $date = $this->input->post('date');



  $result = $this->Insert->ShipmentDetails($date);
  
  $data['result']=$result;


   $this->load->library('pdf');
   $data['sent']=$this->Insert->companyprofile();
   $this->pdf->load_view('reports_area/ShipmentDetailsreport',$data);  
 
}


public function shipmentdetails()
{
    $this->loadViews("shipmentdetails",$this->global);
}


public function All_Report()
{
 
$this->load->model("Insert");

 $data['repot']=$this->Insert->getdeal();

$this->loadViews("all_report",$this->global,$data);


}

          public function Destination()
      {


       $this->loadViews("Destination",$this->global);



      }      

        public function insertprogram(){

        $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->insert_program($data);
        if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('program'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('program'));    
        }
            
        }

        public function editprogram(){
            $id = $_GET['id'];
            $data['all_pro'] = $this->Main_Model->upd_pro($id);
    $this->loadViews("program/editprogramform", $this->global, $data, NULL);
        }

        public function updateprogram(){
            $data= $this->input->post();
        $this->load->model('Main_Model');
        $query = $this->Main_Model->update_program($data);
        if($query==TRUE)
        {
            $this->session->set_flashdata('success','Record is inserted..');
            redirect(base_url('program'));
        }
        else
        {
            $this->session->set_flashdata('error','Record is not inserted..yet');
            redirect(base_url('program'));    
        }
        }

        public function deleteprogram(){
            $id = $_GET['id'];
           $query = $this->Main_Model->delete_pro($id);
        if($query == TRUE)
        {
            $this->session->set_flashdata('success','Record is deleted..');
            redirect(base_url('program'));    
        }
        else
        {
            $this->session->set_flashdata('error','Record is not deleted..yet');
            redirect(base_url('program'));    
        
        }


        }
       






    //MODULE

    
    
    //COURSESTART


    

         //$this->course();
    


   








    /**
     * This function is used to load the user list
     */
    function userListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( "userListing/", $count, 10 );
            
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : User Listing';
            
            $this->loadViews("users", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = 'CodeInsect : Add New User';
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $query = $this->db->query('SELECT * FROM location');
            $data['location'] = $query->result();

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');

            $this->form_validation->set_rules('email','Email','trim|required|is_unique[tbl_users.email]|valid_email|max_length[128]');

            $this->form_validation->set_rules('password','Password','required|max_length[20]');

            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');

            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|is_unique[tbl_users.mobile]|min_length[10]');
             $this->form_validation->set_rules('location_id','Location','trim|required|numeric');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {

              echo  $location = $this->input->post('location_id');
             

                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,
                                    'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'), 'location_id' => $location);
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('userListing');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'CodeInsect : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                
                $userInfo = array();
                
                if(empty($password))
                {
                    $userInfo = array('email'=>$email, 'roleId'=>$roleId, 'name'=>$name,
                                    'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                else
                {
                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'name'=>ucwords($name), 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 
                        'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $userId = ($userId == NULL ? 0 : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 10, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : User Login History';
            
            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }        
    }

    /**
     * This function is used to show users profile
     */
    function profile($active = "details")
    {
        $data["userInfo"] = $this->user_model->getUserInfoWithRole($this->vendorId);
        $data["active"] = $active;
        
        $this->global['pageTitle'] = $active == "details" ? 'CodeInsect : My Profile' : 'CodeInsect : Change Password';
        $this->loadViews("profile", $this->global, $data, NULL);
    }

    /**
     * This function is used to update the user details
     * @param text $active : This is flag to set the active tab
     */
    function profileUpdate($active = "details")
    {
        $this->load->library('form_validation');
            
        $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
        $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
            $mobile = $this->security->xss_clean($this->input->post('mobile'));
            
            $userInfo = array('name'=>$name, 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->editUser($userInfo, $this->vendorId);
            
            if($result == true)
            {
                $this->session->set_userdata('name', $name);
                $this->session->set_flashdata('success', 'Profile updated successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Profile updation failed');
            }

            redirect('profile/'.$active);
        }
    }

    /**
     * This function is used to change the password of the user
     * @param text $active : This is flag to set the active tab
     */
    function changePassword($active = "changepass")
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect('profile/'.$active);
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('profile/'.$active);
            }
        }
    }
}

?>