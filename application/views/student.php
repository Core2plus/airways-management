<style>
.inv{
  display: none;
tr.something
{
td{
  width:1200px;
}
}
}


  .modal-header, h4, .close {
      background-color:   #4682B4;
      color:white !important;
      text-align: center;
      font-size: 30px;
  }
  .modal-footer {
      background-color: #f9f9f9;
  }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> All Student
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url() ?>addstd"><i class="fa fa-plus"></i> Add New</a>
                    <a class="btn btn-primary" href="<?php echo base_url().'charts';?>"><i class="fa fa-pie-chart"></i>Chart/ Records</a>
                </div>
            </div>
        </div>

        
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Students</h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">




    <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                       <th>ID</th>
                                        <th>ARTT ID</th>
                                        <th>CR#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Batch Name</th>
                                         <th>Phone</th>
                                        
                                        
                                       
                        
                         <th class="text-center">Actions</th> 
                    </tr>
                </thead>
                <tbody>
                 <?php
                  $no = 1;
                   foreach ($students->result() as $student) {
                     
                   ?>
               
                <tr>



                   <td><?php echo $no++;  ?>
 <button class="btn btn-danger btn-xs" id="myBtn" onclick="myfun(<?php echo $no;?>)">
  <i class="fa fa-plus" style="font-size:10px;"></i> </button>


</td>
                                        
                           <td><?php echo $student->artt_id;   ?> </td>
                       <td><?php echo $student->cr;   ?> </td>
             <td> <?php echo $student->fname;  ?> </td>
                 <td> <?php echo $student->email;  ?> </td>
                 <td style = "width : 20px" > <?php
                   if($student->fstatus=="UnFreeze")
                                                {

                                                  echo "Regular";
                                                }
                                                if($student->fstatus=="Freeze")
                                                {

                                                  echo "Freezed";
                                                }

                                                  ?>

                                              </td>
                                       <td> 
                                            <span style = "font-size : 14px; ">


                                            <!-- <a href="<?php /*echo base_url('Main/show_enrolled_student/').*/ echo $student->studentid; ?>"> --><?php echo $student->batch_name;  ?>
                                            </span>
                                            </td>

                                            <td>   
                                                 <?php echo $student->phone;  ?>
                                               </td>
                                             

                                         
                                       
                                        
        <td class="text-center">
                                                                  
        <a class="btn btn-sm btn-info" href="<?php echo base_url('editstd') ?>?id=<?php echo $student->studentid ?>" title="Edit"><i class="fa fa-pencil"></i> </a>
        
         <a class="btn btn-sm btn-info" href="<?php echo base_url('profilestd') ?>?id=<?php echo $student->studentid ?>" title="Profile"><i class="fa fa-fw fa-user"></i> </a>

          <a class="btn btn-sm btn-info" href="<?php echo base_url('enrollestuden') ?>?id=<?php echo $student->studentid ?>" title="Enrolle Student"><i class="fa fa-fw fa-user-plus"></i> </a>

           <a class="btn btn-sm btn-info" href="<?php echo base_url('stdvoucher') ?>?id=<?php echo $student->studentid ?>" title="Voucher"><i class="fa fa-fw fa-vimeo-square"></i> </a>

          <a class="btn btn-sm btn-info" href="<?php echo base_url('disscountInsert') ?>?id=<?php echo $student->studentid ?>" title="Disscount"><i class="fa fa-fw fa-money"></i> </a>
                      
            <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url('delete_student') ?>?id=<?php echo $student->studentid ?>" data-userid="" title="Delete">
            <i class="fa fa-trash"></i>
        </a>
                        </td>
                  
                </tr>

                 <?php } ?>
                </tbody>
                
              </table>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="width:800px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="text-center">Parent Information</h1>
        </div>
        <div class="modal-body" style="padding:40px 50px;" id="paragraph<?php echo $no;?>"class="inv">
         <div class="table-responsive">          
  <table class="table">
    <thead>
        <tr>
                       
                    <td></td>
                     <td align="center"><b>Father Name</b></td>
                     <td align="center"><b>Father Email</b></td>
                     <td align="center"> <b>Father_Cnic</b></td>
                     <td align="center"><b>Father_Phone</b></td>
                     <td align="center"><b>Father_Profession</b></td>
                     <td align="center"><b>Permanent_Address </b></td>

                      </tr>
    </thead>
    <tbody>
         <p style="margin-top:10px;">
   
     <td></td>
     
 <td align="center"> <?php echo $student->fname; ?></td>
 <td align="center"> <?php echo $student->father_email; ?></td> 
<td align="center"> <?php echo $student->father_cnic;?></td>
  <td align="center"><?php echo $student->father_phone;?></td>
  <td align="center"><?php echo $student->father_profession;?></td>
 <td align="center"><?php echo $student->permanent_address;?></td> 

</p>
    </tbody>
  </table>
  </div>
        </div>
        <div class="modal-footer">

          
        </div>
      </div>
      
    </div>

  </div> 

</div>








                  
                    
                
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script>
function myfun(e)
{
 
 p=document.getElementById("paragraph"+e);
 $(p).toggle("fade");

}

</script>



<script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















