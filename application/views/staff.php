<style>
      .content-wrapper {
      background:url("<?php base_url(); ?>assets/images/back02.jpg");
    
   
     
    
      

      }

</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="color:black;">
        <i class="fa fa-users"></i> staff Management
        <small style="color:black;">Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
     <a class="btn btn-primary" href="<?php echo base_url(); ?>addNewstaff"><i class="fa fa-plus"></i> Add New</a>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box"style="background-color:#B8860B; opacity:0.9;">
                <div class="box-header">
                    <h3 class="box-title"style="color:black;font-weight:bold;">Users List</h3>
                    <div class="box-tools">
                        
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="table-responsive">
                  <table class="table" id="example1">
                    <thead style="color:black;">
                    <tr>
                        <th>staff_name</th>
                        <th>middle_name</th>
                        <th>last_name</th>
                        <th>email</th>
                        <th>phone</th>
                          <th>address</th>
                        <th>city</th>
                        <th>cnic</th>
                        <th>gender</th>
                        <th>dob</th>
                          <th>education</th>
                        <th>designation</th>
                        <th>salary</th>
                        <th>Image</th>
                        <th>action</th>
                        <th></th>
                       
                      
                      
                    </tr>
                    </thead>
                    <tbody style="color:black;">

<?php

foreach ($fatch as $val) {
 



?>


                    <tr>
                        <td> <?php echo $val->staff_name;   ?>  </td>
                        <td><?php echo $val->middle_name;   ?></td>
                        <td><?php echo $val->last_name;   ?></td>
                        <td><?php echo $val->email;   ?></td>
                        <td><?php echo $val->phone;   ?></td>
                       <td> <?php echo $val->address;   ?> </td>
                        <td><?php echo $val->city;   ?></td>
                        <td><?php echo $val->cnic;   ?></td>
                        <td><?php echo $val->gender;   ?></td>
                        <td><?php echo $val->dob;   ?></td>
                        <td> <?php echo $val->education;   ?>  </td>
                        <td><?php echo $val->designation;   ?></td>
                        <td><?php echo $val->salary;   ?></td>
<td>  <img src="<?php echo base_url() .'upload/'.$val->image; ?>" class="img-responsive"> </td>
                        
                        <td>
    <form action="<?php echo base_url().'user/staff_edit';?>" method="POST">
<input type="hidden" name="name" value="<?php echo $val->staff_id;?>">
<input type="submit" class="btn btn-primary" style="margin-left:15px;"value="Edit"> 
</form>







</td>
<td>
<a class="btn btn-danger"
 href="<?php echo base_url().'user/staffdel/'.$val->staff_id; ?>" title="Delete">delete</a>


                        </td>
                       
                        






                        
                    </tr>
                    <?php
                }
                ?>
                   
                    </tbody>
                  </table>
                  </div>
                </div><!-- /.box-body -->
               
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
