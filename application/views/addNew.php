
<style>
      .content-wrapper {
      background:url("<?php base_url(); ?>assets/images/back02.jpg");
    
   
     
    
      

      }

</style>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="color:black;font-weight:bold;">
        <i class="fa fa-users"></i> User Management
        <small>Add / Edit User</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-2">
              <!-- general form elements -->
                </div>
                <div class="col-md-8">
                <br>
                
                <div class="box"style="background-color:#B8860B; opacity:0.9;">
                    <div class="box-header">
                        <h3 class="box-title"  style="color:black;font-weight:bold;">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
 
                    <form role="form" id="addUser" action="<?php echo base_url() ?>addNewUser" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname"  style="color:black;">Full Name</label>
<input type="text" class="form-control required" value="<?php echo set_value('fname'); ?>" id="fname"  placeholder="Enter full name"   name="fname" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group"  style="color:black;">
                                        <label for="email">Email address</label>
<input type="text" class="form-control required email" id="email" value="<?php echo set_value('email'); ?>" placeholder="Enter email" name="email" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password"  style="color:black;">Password</label>
        <input type="password" class="form-control required" id="password" name="password" maxlength="20" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword"  style="color:black;">Confirm Password</label>
    <input type="password" class="form-control required equalTo" id="cpassword" name="cpassword" maxlength="20" placeholder="Confirm password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile"  style="color:black;">Mobile Number</label>
                 <input type="text" class="form-control required digits" id="mobile" value="<?php echo set_value('mobile'); ?>" name="mobile" maxlength="10" placeholder="Enter mobile Number">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role"  style="color:black;">Role</label>
                                        <select class="form-control required" id="role" name="role">
                                            <option value="0">Select Role</option>
                                            <?php
                                            if(!empty($roles))
                                            {
                                                foreach ($roles as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->roleId ?>" <?php if($rl->roleId == set_value('role')) {echo "selected=selected";} ?>><?php echo $rl->role ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>  



                                <!-- Location -->

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role"  style="color:black;">Location</label>
                                        <select class="form-control required" id="Location" name="location_id">
                                            <option value="0">Select Location</option>
                                            <?php
                                            if(!empty($location))
                                            {
                                                foreach ($location as $row)
                                                {
                                                    ?>
                                                    <option value="<?php echo $row->LocationID; ?>" > <?php echo $row->LocationName; ?> </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>  


                                <!-- Location -->


                                
                            </div>
                               <div >







     <input type="submit" class="btn btn-primary" value="Submit" >
     <input type="reset" class="btn btn-default" value="Reset" >
                        </div>  
                        </div><!-- /.box-body -->
    
                      
                         
                        
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>